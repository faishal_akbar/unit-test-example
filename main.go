package main

import (
	"fmt"
	"unittesting/student"
	"unittesting/studentdb"
)

func main() {
	studentDB := studentdb.InitStudentDB()
	studentBusinessCase := student.InitStudent(studentDB)
	data, err := studentBusinessCase.GetStudentByID(2)

	fmt.Printf("data=%s, err=%v", data, err)
}
