module unittesting

go 1.16

require (
	github.com/golang/mock v1.6.0
	github.com/smartystreets/goconvey v1.7.2
)
