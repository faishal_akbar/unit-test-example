package student

import (
	"testing"

	mock_studentdb "unittesting/mock/studentdb"

	"github.com/golang/mock/gomock"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPositiveCase(t *testing.T) {
	Convey("Testing Positive Case", t, func() {
		// 1. Build Controller and related dependency
		mockController := gomock.NewController(t)
		studentDBMock := mock_studentdb.NewMockStudentDB(mockController)
		studentService := InitStudent(studentDBMock)

		// 2. Create Assertion for DB
		// in this case, inputting ID 3 returning name "Adam"

		// Even though ID 3 hasn't declared on the student/student.go, we can still make it works because
		// we already make the expectation based on the given parameter.

		// Example Expectation
		studentDBMock.
			EXPECT().             // Build Expectation
			GetStudent(int64(3)). // When this function called with these parameter
			Return("Adam", nil).  // Return this data
			Times(1)              // But only for {n} many times

		// 3. Execute Function related
		data, err := studentService.GetStudentByID(3)

		// 4. Assert it by using assertion framework
		So(data, ShouldEqual, "Adam")
		So(err, ShouldBeNil)
	})
}
