package student

import "unittesting/studentdb"

type student struct {
	db studentdb.StudentDB
}

type Student interface {
	GetStudentByID(id int64) (string, error)
}

func InitStudent(db studentdb.StudentDB) Student {
	return &student{
		db: db,
	}
}

func (s *student) GetStudentByID(id int64) (string, error) {
	data, err := s.db.GetStudent(id)
	if err != nil {
		return "", err
	}

	return data, nil
}
