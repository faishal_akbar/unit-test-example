package studentdb

import "errors"

type studentDB struct {
}

type StudentDB interface {
	GetStudent(id int64) (string, error)
}

func InitStudentDB() StudentDB {
	return &studentDB{}
}

func (s *studentDB) GetStudent(id int64) (string, error) {
	switch id {
	case 1:
		return "Adhi", nil
	case 2:
		return "Nico", nil
	}

	return "", errors.New("no student defined")
}
