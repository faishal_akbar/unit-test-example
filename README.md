# Unit Test Example

## How to run
* `go mod tidy`

## How to generate mock?
You can use `mockgen` to generate mock based on the interface that are available in `student/` and `studentdb/`

```
`go env GOPATH`/bin/mockgen -source student/student.go -destination mock/student/student.go  
`go env GOPATH`/bin/mockgen -source studentdb/studentdb.go -destination mock/studentdb/studentdb.go  
```

## What's next?
Check code at `student/student_test.go` to understand Unit Testing at glance.